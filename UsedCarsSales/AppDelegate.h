//
//  AppDelegate.h
//  UsedCarsSales
//
//  Created by SeongHyeon Jeon on 12. 7. 15..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
