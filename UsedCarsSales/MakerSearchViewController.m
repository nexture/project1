//
//  MakerSearchViewController.m
//  UsedCarsSales
//
//  Created by 金 美眞 on 12/09/05.
//
//

#import "MakerSearchViewController.h"
#import "ToyotaViewController.h"

@interface MakerSearchViewController ()

@end

@implementation MakerSearchViewController

NSArray *makerArray;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    makerArray = [NSArray arrayWithObjects:@"トヨタ",@"レクサス",@"日産",@"ホンダ",@"マツダ",@"三菱",@"スバル",@"ダイハツ",@"スズキ",@"ワーゲン",@"アウディ",@"Mベンツ",@"BMW",@"GM",@"その他", nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    makerArray = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [makerArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    NSUInteger section = [indexPath section];
    if (section == 0){
        cell.textLabel.text = [makerArray objectAtIndex:indexPath.row];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = [indexPath section];
    if (section == 0){
        if (indexPath.row == 0){
            ToyotaViewController *toyotaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Toyota"];
            toyotaViewController.title = [makerArray objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:toyotaViewController animated:YES];
        }
    }
}

@end
