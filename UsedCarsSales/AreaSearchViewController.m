//
//  MakerSearchViewController.m
//  UsedCarsSales
//
//  Created by 金 美眞 on 12/09/05.
//
//

#import "AreaSearchViewController.h"

@interface AreaSearchViewController ()

@end

@implementation AreaSearchViewController

NSArray *areaArray;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    areaArray = [NSArray arrayWithObjects:@"北海道",@"青森県",@"岩手県",@"宮城県",@"秋田県",@"山形県",@"福島県",@"東京都",@"神奈川県",@"埼玉県",@"千葉県",@"茨城県",@"栃木県",@"群馬県",@"山梨県",@"新潟県",@"富山県",@"長野県",@"石川県",@"福井県",@"岐阜県",@"静岡県",@"愛知県",@"三重県",@"京都府",@"大阪府",@"滋賀県",@"兵庫県",@"奈良県",@"和歌山県",@"鳥取県",@"島根県", @"岡山県",@"広島県",@"山口県",@"徳島県",@"香川県",@"愛媛県",@"高知県",@"福岡県",@"佐賀県",@"長崎県",@"熊本県",@"大分県",@"宮崎県",@"鹿児島県",@"沖縄県",nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    areaArray = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [areaArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    NSUInteger section = [indexPath section];
    if (section == 0){
        cell.textLabel.text = [areaArray objectAtIndex:indexPath.row];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = [indexPath section];
    
    /*
    if (section == 0){
        if (indexPath.row == 0){
            TokyoViewController *tokyoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"hukaido"];
            tokyoViewController.title = [areaArray objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:tokyoViewController animated:YES];
        }
    }
     */
}

@end
