//
//  TableViewController.m
//  NavigationTest
//
//  Created by Mijin Kim on 12/05/13.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TableViewController.h"
#import <sqlite3.h>

@implementation TableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    mdic = [NSMutableDictionary dictionary];
    
    sqlite3 *database;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"MemberInfo" ofType:@"sqlite"];
    if (sqlite3_open([path UTF8String], &database) == SQLITE_OK){
        NSString *query = [NSString stringWithFormat:@"select no, name, sex, birthDay, enterDay, tel, email from memberDbData"];
        const char *sql = [query UTF8String];
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK){
            while (sqlite3_step(statement) == SQLITE_ROW) {          
                NSMutableArray *unsortedArray = [NSMutableArray array]; 
                NSString *key;                                              
                char* no = (char*)sqlite3_column_text(statement, 0);
                if (no != NULL){
                    key = [NSString stringWithUTF8String:no];
                }            
                char* name = (char*)sqlite3_column_text(statement, 1);
                if (name != NULL){
                    [unsortedArray addObject:[NSString stringWithUTF8String:name]];
                }
                char* sex = (char*)sqlite3_column_text(statement, 2);
                if (sex != NULL){
                    [unsortedArray addObject:[NSString stringWithUTF8String:sex]];
                }
                char* birthDay = (char*)sqlite3_column_text(statement, 3);
                if (birthDay != NULL){
                    [unsortedArray addObject:[NSString stringWithUTF8String:birthDay]];
                }
                char* enterDay = (char*)sqlite3_column_text(statement, 4);
                if (enterDay != NULL){
                    [unsortedArray addObject:[NSString stringWithUTF8String:enterDay]];
                }
                char* tel = (char*)sqlite3_column_text(statement, 5);                                                                                                                                                           
                if (tel != NULL){
                    [unsortedArray addObject:[NSString stringWithUTF8String:tel]];
                }
                char* email = (char*)sqlite3_column_text(statement, 6);
                if (email != NULL){
                    [unsortedArray addObject:[NSString stringWithUTF8String:email]];
                }                
                [mdic setObject:unsortedArray forKey:key];
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(database); 
    
//    for(int i=0; i<100; i++){
//        NSMutableArray *unsortedArray = [NSMutableArray array];
//        NSString *memberid = [NSString stringWithFormat:@"%d", i+1000];
//        NSString *name = @"name";
//        NSString *sex = @"sex";
//        NSString *birthDay = @"birthDay";
//        NSString *enterDay = @"enterDay";
//        NSString *tel = @"tel";
//        NSString *email = @"email";
//        [name stringByAppendingString:[NSString stringWithFormat:@"%d", i]]; 
//        [unsortedArray addObject:name];
//        [unsortedArray addObject:sex];
//        [unsortedArray addObject:birthDay];
//        [unsortedArray addObject:enterDay];
//        [unsortedArray addObject:tel];
//        [unsortedArray addObject:email];
//        [mdic setObject:unsortedArray forKey:memberid];
//    }
    //sortedArray = [unsortedKeyArray sortedArrayUsingSelector:@selector(caseInsensitiveCompare)];
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int count = [[mdic allKeys] count];
    NSLog(@"test");

    NSLog(@"count=%d", count);
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"memberInfo";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...    
    //cell.textLabel.text = [NSString stringWithFormat:@"%d", indexPath.row + 1001];
    NSArray *array1 = [mdic allKeys];
    cell.textLabel.text = [array1 objectAtIndex:indexPath.row];
    NSArray *array2 = [mdic objectForKey:cell.textLabel.text];
    cell.detailTextLabel.text = [array2 objectAtIndex:0];
    cell.imageView.image = [UIImage imageNamed:@"606685.jpg"];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
