//
//  TableViewController.h
//  NavigationTest
//
//  Created by Mijin Kim on 12/05/13.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController{
    
    NSMutableDictionary *mdic;
    NSArray *array;
    NSArray *sortedArray;
    
}

@end
