//
//  HomeViewController.h
//  UsedCarsSales
//
//  Created by SeongHyeon Jeon on 12. 7. 15..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
{
    IBOutlet UIScrollView *carScrollView;
}
@property (strong, nonatomic) IBOutlet UIScrollView *carScrollView;
@end
