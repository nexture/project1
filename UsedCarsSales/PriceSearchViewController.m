//
//  PriceSearchViewController.m
//  UsedCarsSales
//
//  Created by 金 美眞 on 12/09/09.
//
//

#import "PriceSearchViewController.h"

@interface PriceSearchViewController ()

@end

@implementation PriceSearchViewController

NSArray *priceArray;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    priceArray = [NSArray arrayWithObjects:@"〜50万円",@"50〜100万円",@"100〜150万円",@"150〜200万円",@"200〜300万円",@"300〜400万円",@"400〜500万円",@"500〜600万円",@"600〜700万円",@"700〜800万円",@"800〜900万円",@"900〜1000万円",nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    priceArray = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [priceArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    NSUInteger section = [indexPath section];
    if (section == 0){
        cell.textLabel.text = [priceArray objectAtIndex:indexPath.row];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = [indexPath section];
    /* 
     if (section == 0){
        if (indexPath.row == 0){
            ToyotaViewController *toyotaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Toyota"];
            toyotaViewController.title = [priceArray objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:toyotaViewController animated:YES];
        }
    }
     */
     
}

@end
