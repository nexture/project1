//
//  SearchViewController.m
//  UsedCarsSales
//
//  Created by 金 美眞 on 12/08/18.
//
//

#import "SearchViewController.h"
#import "SearchDetailViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

NSArray *searchArray;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    searchArray = [NSArray arrayWithObjects:@"メーカーで探す", @"地域で探す", @"価格帯で探す",@"年式で探す", @"こだわりで探す", nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    searchArray = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [searchArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    NSUInteger section = [indexPath section];
    if (section == 0){
        cell.textLabel.text = [searchArray objectAtIndex:indexPath.row];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = [indexPath section];
    if (section == 0){
        if (indexPath.row == 0){
            SearchDetailViewController *detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MakerSearch"];
            detailViewController.title = [searchArray objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
        
        if (indexPath.row == 1){
            SearchDetailViewController *detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AreaSearch"];
            detailViewController.title = [searchArray objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
        
        if (indexPath.row == 2){
            SearchDetailViewController *detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PriceSearch"];
            detailViewController.title = [searchArray objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
    }
}

@end
